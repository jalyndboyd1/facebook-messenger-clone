import React, { forwardRef } from "react";
import "./Message.css";
import { Card, CardContent, Typography } from "@material-ui/core";

const Message = forwardRef(({ message, username }, ref) => {
  const isUser = username === message.username;

  return (
    // the div that the element is keeping track of.
    <div ref={ref} className={`message ${isUser && "message-user"}`}>
      {/* Only the logged in person receives the message user styling */}
      <Card className={isUser ? "message-usercard" : "message-guestcard"}>
        <CardContent>
          <Typography color="white" variant="h5" component="h2">
            {message.username}:{message.message}
          </Typography>
        </CardContent>
      </Card>
    </div>
  );
});

export default Message;
