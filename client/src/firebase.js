import firebase from "firebase";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAIf7cBuHRL5IU6Fl0xWaYZDWIQX-3x2pE",
  authDomain: "facebook-messenger-272f7.firebaseapp.com",
  projectId: "facebook-messenger-272f7",
  storageBucket: "facebook-messenger-272f7.appspot.com",
  messagingSenderId: "69461278513",
  appId: "1:69461278513:web:e1e4ca556d4887ea853a10",
  measurementId: "G-4DG90R6ZFG",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();

export default db;
