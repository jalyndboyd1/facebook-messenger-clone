import "./App.css";
import React, { useState, useEffect } from "react";
import {
  Button,
  FormControl,
  InputLabel,
  Input,
  IconButton,
} from "@material-ui/core";
import Message from "./Message";
import db from "./firebase";
import firebase from "firebase";
import FlipMove from "react-flip-move";
import SendIcon from "@material-ui/icons/Send";

function App() {
  const [input, setInput] = useState("");
  const [messages, setMessages] = useState([]);
  const [username, setUsername] = useState("");

  useEffect(() => {
    // Logic for the database.
    db.collection("messages")
      .orderBy("timestamp", "desc")
      .onSnapshot((snapshot) => {
        setMessages(
          snapshot.docs.map((doc) => {
            return { id: doc.id, message: doc.data() };
          })
        );
      });
  }, []);
  useEffect(() => {
    setUsername(prompt("Please Enter Your Name"));
  }, []);

  const sendMessage = (event) => {
    event.preventDefault();
    db.collection("messages").add({
      message: input,
      username: username,
      timestamp: firebase.firestore.FieldValue.serverTimestamp(),
    });
    // keep all of the messages and append input to the end.
    setInput("");
  };

  return (
    <div className="app">
      <img
        src="https://images-platform.99static.com//AknNMPcFpBoqWC-Nz13E39NRNzM=/85x2484:1110x3508/fit-in/500x500/99designs-contests-attachments/118/118658/attachment_118658622"
        alt=""
      />
      <h2>Welcome, {username}</h2>
      <div className="app-form">
        <FormControl>
          <InputLabel>Shoot your shot, broh! ... </InputLabel>
          <Input
            type="text"
            value={input}
            onChange={(e) => setInput(e.target.value)}
          />
          <IconButton
            variant="contained"
            color="primary"
            type="submit"
            onClick={sendMessage}
            disabled={!input}
          >
            <SendIcon />
          </IconButton>
        </FormControl>
      </div>

      <FlipMove>
        {messages.map(({ message, id }) => {
          return <Message key={id} message={message} username={username} />;
        })}
      </FlipMove>
    </div>
  );
}

export default App;
